import React, { useContext, useState } from 'react';
import { useGlobalState, GlobalState } from '../GlobalState';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import CircularProgress from '@mui/material/CircularProgress';
import DeleteIcon from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';
export default function Footer() {
  const [checkAll, setCheckAll] = useState(false);
  const { todos } = useGlobalState();

  const setTodos = (newTodos) => {
    GlobalState.set({ todos: newTodos });
  };

  const handleCheckAll = () => {
    const newTodos = [...todos];
    newTodos.forEach((todo) => {
      todo.complete = !checkAll;
    });
    setTodos(newTodos);
    setCheckAll(!checkAll);
  };

  const newTodosComplete = () => {
    console.log(todos);
    return todos.filter((todo) => todo.complete === false);
  };

  const deleteTodo = () => {
    setTodos(newTodosComplete());
    setCheckAll(false);
  };

  const countProgress = () => {
    const total = todos.length;
    const solved = total - newTodosComplete().length;
    return (solved / total) * 100;
  };

  return (
    <Grid
      container
      direction="row-reverse"
      justifyContent="center"
      alignItems="center"
    >
      {todos.length === 0 ? (
        <h2>Congratulations! Nothings To Do</h2>
      ) : (
        <div style={{ textAlign: 'center' }}>
          <label htmlFor="all">
            <input
              type="checkbox"
              name="all"
              id="all"
              onChange={handleCheckAll}
              checked={checkAll}
            />
            Select All
          </label>
          <Box>
            <CircularProgress variant="determinate" value={countProgress()} />
            <p>{newTodosComplete().length} left</p>
          </Box>

          <Button
            style={{
              marginLeft: '20px',
              marginTop: '10px',
              padding: '15px',
              width: '150px',
            }}
            size="small"
            variant="outlined"
            startIcon={<DeleteIcon />}
            color="error"
            id="delete"
            onClick={deleteTodo}
          >
            Delete
          </Button>
        </div>
      )}
    </Grid>
  );
}
