import React, { useState, useContext, useRef, useEffect } from 'react';
import { useGlobalState, GlobalState } from '../GlobalState';
import AddIcon from '@mui/icons-material/Add';
import Button from '@mui/material/Button';
import dayjs from 'dayjs';
import Box from '@mui/material/Box';
import { DatePicker } from '@mui/lab';

function TodayDate() {
  const today = new Date();
  const year = today.getFullYear();
  const month = String(today.getMonth() + 1).padStart(2, '0');
  const day = String(today.getDate()).padStart(2, '0');

  return `${year}-${month}-${day}`;
}

export default function FormInput() {
  const { todos } = useGlobalState();

  const setTodos = (newTodos) => {
    GlobalState.set({ todos: newTodos });
  };
  const [todoName, setTodoName] = useState('');
  const todoInput = useRef();
  const [date, setNewDate] = useState(TodayDate());

  const addTodo = (e) => {
    e.preventDefault();
    setTodos([...todos, { name: todoName, complete: false }]);
    setTodoName('');
    todoInput.current.focus();
  };

  useEffect(() => {
    todoInput.current.focus();
  }, []);

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
      }}
    >
      <form autoComplete="off" onSubmit={addTodo}>
        <input
          style={{
            width: '200px',
            height: '10px',
            border: '1px solid #ccc',
            padding: '10px',
            fontSize: '15px',
          }}
          type="text"
          name="todos"
          id="todos"
          ref={todoInput}
          required
          placeholder="What needs to be done?"
          value={todoName}
          onChange={(e) => setTodoName(e.target.value.toLowerCase())}
        />

        <Button
          style={{ marginLeft: '50px', height: '35px' }}
          size="small"
          type="submit"
          variant="outlined"
          startIcon={<AddIcon />}
        >
          Add
        </Button>
      </form>
    </div>
  );
}
