import React from 'react';
import List from './List';
import Footer from './Footer';
import FormInput from './FormInput';
import Grid from '@mui/material/Grid';
import TableOfContent from './TableOfContent';

const Layout = () => {
  return (
    <div>
      <TableOfContent />
      <div>
        <List></List>
        <Grid
          container
          direction="row-reverse"
          justifyContent="center"
          alignItems="center"
          padding={6}
        >
          <FormInput></FormInput>
          <Footer />
        </Grid>
      </div>
    </div>
  );
};

export default Layout;
