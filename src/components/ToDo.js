import React, { useState } from 'react';
import { Card, CardActions } from '@mui/material';

import { OutlinedInput } from '@mui/material';
import Button from '@mui/material/Button';
import { green } from '@mui/material/colors';

export default function ToDo({ todo, id, checkComplete, handleEditTodos }) {
  const [editValue, setEditValue] = useState('');

  const handleOnEdit = () => {
    setEditValue(todo.name);
  };

  const handleSave = () => {
    if (editValue) {
      handleEditTodos(editValue, id);
      setEditValue('');
    } else {
      setEditValue(todo.name);
    }
  };

  return (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <div style={{ padding: '5px', marginRight: '35px', width: '350px' }}>
        <Card variant="outlined">
          <li>
            <CardActions>
              {todo.complete ? (
                <label htmlFor={id} className="active">
                  <input
                    type="checkbox"
                    id={id}
                    checked={true}
                    onChange={() => checkComplete(id)}
                    disabled
                  />
                  {todo.name}
                </label>
              ) : (
                <>
                  <input
                    type="checkbox"
                    id={id}
                    checked={false}
                    onChange={() => checkComplete(id)}
                  />
                  {editValue ? (
                    <input
                      type="text"
                      id="editValue"
                      value={editValue}
                      name="editValue"
                      onChange={(e) =>
                        setEditValue(e.target.value.toLowerCase())
                      }
                    />
                  ) : (
                    <span>{todo.name}</span>
                  )}
                  {editValue ? (
                    <Button onClick={handleSave} size="small">
                      Save
                    </Button>
                  ) : (
                    <Button onClick={handleOnEdit} size="small">
                      Edit
                    </Button>
                  )}
                </>
              )}
            </CardActions>
          </li>
        </Card>
      </div>
    </div>
  );
}
