import React from 'react';
import TableOfContent from './TableOfContent';
export default function Inbox() {
  return (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <TableOfContent content={['one', 'two', 'three']} />
      <h1>Inbox</h1>
    </div>
  );
}
