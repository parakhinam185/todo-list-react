import React from 'react';
import { NavLink } from 'react-router-dom';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import DashboardIcon from '@mui/icons-material/Dashboard';
import ListItemText from '@mui/material/ListItemText';
import Toolbar from '@mui/material/Toolbar';

export default function Navbar() {
  const drawerWidth = 240;
  return (
    <div>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <Box component="nav" sx={{ width: drawerWidth, flexShrink: 0 }}>
          <Drawer
            variant="permanent"
            sx={{
              width: drawerWidth,
              flexShrink: 0,
              '& .MuiDrawer-paper': {
                width: drawerWidth,
                boxSizing: 'border-box',
              },
            }}
            open={true}
          >
            <Toolbar />
            <React.Fragment>
              <List>
                <ListItemButton component={NavLink} to="/">
                  <ListItemIcon>
                    <DashboardIcon />
                  </ListItemIcon>
                  <ListItemText primary="ToDo" />
                </ListItemButton>

                <ListItemButton component={NavLink} to="/inbox">
                  <ListItemIcon>
                    <DashboardIcon />
                  </ListItemIcon>
                  <ListItemText primary="Inbox" />
                </ListItemButton>
              </List>
            </React.Fragment>
          </Drawer>
        </Box>
      </Box>
    </div>
  );
}
