import React from 'react';
import { NavLink } from 'react-router-dom';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import DashboardIcon from '@mui/icons-material/Dashboard';
import ListItemText from '@mui/material/ListItemText';
import Toolbar from '@mui/material/Toolbar';
import { Typography } from '@mui/material';

const drawerWidth = 240;

export default function TableOfContent({ content = [] }) {
  return (
    <div>
      {content.length != 0 && (
        <Box sx={{ display: 'flex' }}>
          <CssBaseline />
          <Box component="nav" sx={{ width: drawerWidth, flexShrink: 0 }}>
            <Drawer
              variant="permanent"
              sx={{
                width: drawerWidth,
                flexShrink: 0,
                '& .MuiDrawer-paper': {
                  width: drawerWidth,
                  boxSizing: 'border-box',
                },
              }}
              open={true}
              anchor="right"
            >
              <Toolbar />
              <Typography
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  flexDirection: 'column',
                }}
              >
                Table of content
              </Typography>
              <List style={{ textAlign: 'center' }}>
                {content.map((item) => (
                  <ListItemText primary={item} />
                ))}
              </List>
            </Drawer>
          </Box>
        </Box>
      )}
    </div>
  );
}
