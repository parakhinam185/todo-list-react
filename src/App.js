import React from 'react';
import { Global } from './GlobalState';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Toolbar from '@mui/material/Toolbar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import ListItem from '@mui/material/ListItem';
import Navbar from './components/Navbar';
import Drawer from '@mui/material/Drawer';
import Inbox from './components/Inbox';
import Layout from './components/Layout';

const drawerWidth = 240;

export default function App() {
  return (
    <Global
      Root={() => (
        <div>
          <BrowserRouter>
            <Navbar />
            <Routes>
              <Route path={'/'} element={<Layout />} />
              <Route path={'/inbox'} element={<Inbox />} />
            </Routes>
          </BrowserRouter>
        </div>
      )}
    />
  );
}
